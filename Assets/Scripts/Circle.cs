﻿using UnityEngine;

public sealed class Circle : MonoBehaviour
{
    void OnMouseDown()
    {
        GameManager.Instance.DestroyCircle(gameObject);
    }
}
