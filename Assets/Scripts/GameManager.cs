﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Parse;
using UnityEngine;
using System.Collections;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    //игровые состояния
    private enum GameState
    {
        None,
        Login,
        Register,
        Loading,
        Menu,
        Game,
        Finish,
    }

    private class RankData
    {
        public string Nickname { get; set; }
        public int BestScores { get; set; }
    }

    public static GameManager Instance;

    public GameObject Circle;
    public float SpawnTimer = 0.2f; //период между спавном нового круга
    public int MaxCircleCount = 10; //максимальное кол-во кругов на поле, после которого игра заканчивается

    private GameState _gameState;   //текущее игровое состояние
    private float _lastTime;    //последний раз, когда спавнулся круг
    private int _currentCircleCount; //текущее кол-во кругов

    private int Points { get; set; } //текущее кол-во очков
    private int Best { get; set; } //лучший результат

    public string Nickname = "Player"; //ник игрока
    private readonly List<RankData> _top = new List<RankData>(); //рейтинг

    private string _username = "";
    private string _password = "";

    // Use this for initialization
    void Start()
    {
        Instance = this;

        //начинаем игру с логина
        GoToState(GameState.Login);
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameState == GameState.Game)
        {
            //spawn new enemy
            if (_lastTime < Time.time)
            {
                _lastTime = Time.time + SpawnTimer;
                SpawnCircle();
            }

            //finish game
            if (_currentCircleCount == MaxCircleCount)
            {
                GoToState(GameState.Finish);
            }
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            TestSeries();
        }

        if (Input.GetKeyUp(KeyCode.P))
        {
            TestParallel();
        }
    }

    /// <summary>
    /// Добавляет на сцену круг
    /// </summary>
    private void SpawnCircle()
    {
        var newObj = (GameObject)Instantiate(Circle, new Vector3(Random.Range(-140.0f, 140.0f), Random.Range(-220.0f, 220.0f), 0), Quaternion.identity);
        _currentCircleCount++;
    }

    /// <summary>
    /// Удаляет со сцены круг
    /// </summary>
    /// <param name="obj"></param>
    public void DestroyCircle(GameObject obj)
    {
        Destroy(obj);
        Points++;
        _currentCircleCount--;
    }

    /// <summary>
    /// Меняет игровое состояние
    /// </summary>
    /// <param name="state">новое состояние</param>
    private void GoToState(GameState state)
    {
        switch (state)
        {
            case GameState.Login:
                break;

            case GameState.Register:
                break;

            case GameState.Menu:
                break;

            case GameState.Game:
                Points = 0;
                _lastTime = 0f;
                _currentCircleCount = 0;
                break;

            #region Finish
            case GameState.Finish:
                {
                    foreach (var enemy in GameObject.FindGameObjectsWithTag("Enemy"))
                    {
                        Destroy(enemy);
                    }

                    if (Best < Points)
                    {
                        Best = Points;

                        StartCoroutine(SaveBest());
                    }

                    //обновляем топ

                    //создаем запрос
                    var topQuery =
                        ParseObject.GetQuery("GameScores") //выбираем таблицу для запроса
                            .WhereExists("nickname") //указываем, что должен присутствовать ключ nickname
                            .OrderByDescending("bestScores") //сортируем результат в порядке убывания
                            .Limit(5); //вовзращаем первые 5 записей

                    //выполняем запрос
                    topQuery.FindAsync().ContinueWith(t =>
                    {
                        if (t.IsCompleted)
                        {
                            //получаем ответ
                            var result = t.Result;

                            //очищаем старый топ и заполняем новыми данными
                            _top.Clear();
                            foreach (var topPlayer in result)
                            {
                                _top.Add(new RankData
                                {
                                    Nickname = topPlayer.Get<string>("nickname"),
                                    BestScores = topPlayer.Get<int>("bestScores")
                                });
                            }
                        }
                    });
                }
                break;
            #endregion
        }

        _gameState = state;

        Debug.Log("NewState:" + _gameState);
    }

    private IEnumerator CallInNextUpdate(Action action)
    {
        yield return new WaitForEndOfFrame();
        action();
    }

    private IEnumerator Login()
    {
        GoToState(GameState.Loading);

        //если нет закешированного юзера - выполняем вход
        if (ParseUser.CurrentUser == null)
        {
            var loginTask = ParseUser.LogInAsync(_username, _password);

            while (!loginTask.IsCompleted && !loginTask.IsFaulted)
                yield return null;

            if (loginTask.IsFaulted)
            {
                GoToState(GameState.Login);

                foreach (var currError in loginTask.Exception.InnerExceptions)
                {
                    var error = (ParseException)currError;

                    switch (error.Code)
                    {
                        case ParseException.ErrorCode.UsernameMissing:
                            Debug.LogError("Username not exists!");
                            yield break;
                        case ParseException.ErrorCode.PasswordMissing:
                            Debug.LogError("Password incorrect!");
                            yield break;
                        default:
                            Debug.LogError("Login error:" + error.Code);
                            yield break;
                    }
                }

                yield break;
            }
        }

        var user = ParseUser.CurrentUser;

        if (user.ContainsKey("best"))
        {
            var key = user.Get<string>("best");

            var task = ParseObject.GetQuery("GameScores").GetAsync(key);

            while (!task.IsCompleted && !task.IsFaulted)
                yield return null;

            if (task.IsFaulted)
            {
                foreach (var currError in task.Exception.InnerExceptions)
                {
                    var error = (ParseException)currError;

                    if (error.Code == ParseException.ErrorCode.ObjectNotFound)
                    {
                        Debug.LogError("No obj with id " + key);
                        yield break;
                    }

                    Debug.LogError("Login error:" + error.Code);
                }
            }
            else
            {
                var dbScores = task.Result;

                Best = dbScores.Get<int>("bestScores");

                Debug.Log("best result loaded!");
            }
        }
        //no best result!
        else
        {
            Best = 0;
        }

        GoToState(GameState.Menu);
    }

    private IEnumerator Register()
    {
        GoToState(GameState.Loading);

        var newUser = new ParseUser()
        {
            Username = _username,
            Password = _password,
        };

        var task = newUser.SignUpAsync();

        while (!task.IsCompleted && !task.IsFaulted)
            yield return null;

        if (task.IsFaulted)
        {
            GoToState(GameState.Register);

            foreach (var currError in task.Exception.InnerExceptions)
            {
                var error = (ParseException)currError;

                switch (error.Code)
                {
                    case ParseException.ErrorCode.UsernameTaken:
                        Debug.LogError("Username already exists!");
                        yield break;
                    case ParseException.ErrorCode.EmailTaken:
                        Debug.LogError("Email already exists!");
                        yield break;
                    default:
                        Debug.LogError("Login error:" + error.Code);
                        yield break;
                }
            }

            yield break;
        }

        Debug.Log("User registered! Log in...");

        //зарегались. Теперь выполняем вход
        StartCoroutine(Login());
    }

    private IEnumerator SaveBest()
    {
        ParseObject dbScores;

        var user = ParseUser.CurrentUser;

        if (!user.ContainsKey("best"))
        {
            dbScores = new ParseObject("GameScores");
        }
        else
        {
            dbScores = ParseObject.CreateWithoutData("GameScores", user.Get<string>("best"));
        }

        dbScores["bestScores"] = Best;
        dbScores["nickname"] = Nickname;

        var saveTask = dbScores.SaveAsync();

        while (!saveTask.IsCompleted)
            yield return null;

        //в этой точке метода очки уже сохранены в базе. сохраняем пользователя
        user["best"] = dbScores.ObjectId;
        var userSaveTask = user.SaveAsync();

        while (!userSaveTask.IsCompleted)
            yield return null;

        Debug.Log("Best result saved!");
    }

    private void TestChain()
    {
        var obj = ParseObject.Create("TestClass");
        obj["str"] = "123456789";

        obj.SaveAsync().ContinueWith(t =>
        {
            var query = ParseObject.GetQuery("TestClass").GetAsync(obj.ObjectId);
            return query;
        }).Unwrap().ContinueWith(t =>
        {
            var newObj = t.Result;
            Debug.Log("newObj str:" + newObj["str"]);

            newObj["str"] = "abcd";

            return newObj.SaveAsync();
        }).Unwrap().ContinueWith(t =>
        {
            Debug.Log("newObj saved!");
            //...
        });
    }

    private void TestSeries()
    {
        var list = new List<ParseObject>();

        for (int i = 0; i < 10; i++)
        {
            list.Add(ParseObject.Create("TestClass2"));
        }

        Task task = Task.FromResult(0);

        for (int i = 0; i < list.Count; i++)
        {
            var toSave = i;
            task = task.ContinueWith(t =>
            {
                return list[toSave].SaveAsync();
            }).Unwrap();
        }

        var sw = Stopwatch.StartNew();
        Debug.Log("Start saving...");
        task.ContinueWith(n =>
        {
            sw.Stop();
            Debug.Log("All saved!" + sw.ElapsedMilliseconds);
        });
    }

    private void TestParallel()
    {
        var list = new List<ParseObject>();

        for (int i = 0; i < 10; i++)
        {
            list.Add(ParseObject.Create("TestClass2"));
        }

        var tasks = new List<Task>();

        for (int i = 0; i < list.Count; i++)
        {
            tasks.Add(list[i].SaveAsync());
        }

        var sw = Stopwatch.StartNew();
        Debug.Log("Start saving...");
        Task.WhenAll(tasks).ContinueWith(n =>
        {
            sw.Stop();
            Debug.Log("All saved!" + sw.ElapsedMilliseconds);
        });
    }

    #region Draw

    void OnGUI()
    {
        GUI.Label(new Rect(200, 10, 100, 20), "Best:" + Best);

        if (_gameState == GameState.Login)
            DrawLogin();
        else if (_gameState == GameState.Register)
            DrawRegister();
        else if (_gameState == GameState.Loading)
            DrawLoading();
        else if (_gameState == GameState.Menu)
            DrawMenu();
        else if (_gameState == GameState.Game)
            DrawGame();
        else if (_gameState == GameState.Finish)
            DrawFinish();
    }

    private void DrawLogin()
    {
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, 10, 200, 200));
        GUILayout.BeginVertical();

        GUILayout.Label("Email:");
        _username = GUILayout.TextField(_username);

        GUILayout.Label("Password:");
        _password = GUILayout.PasswordField(_password, '*');

        if (GUILayout.Button("Log in"))
        {
            StartCoroutine(Login());
        }

        if (GUILayout.Button("Register"))
        {
            GoToState(GameState.Register);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    private void DrawRegister()
    {
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, 10, 200, 200));
        GUILayout.BeginVertical();

        GUILayout.Label("Email:");
        _username = GUILayout.TextField(_username);

        GUILayout.Label("Password:");
        _password = GUILayout.PasswordField(_password, '*');

        if (GUILayout.Button("Register"))
        {
            StartCoroutine(Register());
        }

        if (GUILayout.Button("Back"))
        {
            GoToState(GameState.Login);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    private void DrawLoading()
    {
        GUI.Label(new Rect(110, 200, 100, 20), "Loading...");
    }

    private void DrawMenu()
    {
        if (GUI.Button(new Rect(110, 200, 100, 20), "Start"))
        {
            GoToState(GameState.Game);
        }
    }

    private void DrawGame()
    {
        GUI.Label(new Rect(10, 10, 200, 20), "Points:" + Points);
    }

    private void DrawFinish()
    {
        if (GUI.Button(new Rect(110, 200, 100, 20), "Restart"))
        {
            GoToState(GameState.Game);
        }

        //draw ranking
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, 10, 200, 200));
        GUILayout.BeginVertical();

        int counter = 1;
        foreach (var rankData in _top)
        {
            GUILayout.Label("#" + counter + " " + rankData.Nickname + " - " + rankData.BestScores);
            counter++;
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    #endregion
}
